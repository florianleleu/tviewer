/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tviewer;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author floyd
 */
public class Server 
{
    protected String name;
    protected String map;
    protected String gametype;
    protected String ip;
    protected int port;
    protected String version;
    protected int ping;
    protected int playerInGame;
    protected int maxPlayerInGame;
    protected int playerInServer;
    protected int maxPlayerInServer;
    
    protected ArrayList<Player> players;
    
    protected boolean favorite;
    
    

    public Server(String ip)
    {
        this.ip = ip;
        this.port = 8303;
        this.favorite = false;        
    }
    
    public Server(String ip, int port)
    {
        this.ip = ip;
        this.port = port;
        this.favorite = false;        
    }
    
    public Server(String ip, int port, boolean favorite)
    {
        this.ip = ip;
        this.port = port;
        this.favorite = favorite;        
    }

    public String getGametype() {
        return gametype;
    }

    public void setGametype(String gametype) {
        this.gametype = gametype;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public int getMaxPlayerInGame() {
        return maxPlayerInGame;
    }

    public void setMaxPlayerInGame(int maxPlayerInGame) {
        this.maxPlayerInGame = maxPlayerInGame;
    }

    public int getMaxPlayerInServer() {
        return maxPlayerInServer;
    }

    public void setMaxPlayerInServer(int maxPlayerInServer) {
        this.maxPlayerInServer = maxPlayerInServer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPing() {
        return ping;
    }

    public void setPing(int ping) {
        this.ping = ping;
    }

    public int getPlayerInGame() {
        return playerInGame;
    }

    public void setPlayerInGame(int playerInGame) {
        this.playerInGame = playerInGame;
    }

    public int getPlayerInServer() {
        return playerInServer;
    }

    public void setPlayerInServer(int playerInServer) {
        this.playerInServer = playerInServer;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }    
    
    public boolean isFavorite() 
    {
        return favorite;
    }

    public void setFavorite(boolean favorite) 
    {
        this.favorite = favorite;
    }

    public String getIp() 
    {
        return ip;
    }

    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public int getPort() 
    {
        return port;
    }

    public void setPort(int port) 
    {
        this.port = port;
    }
    
    public void init()
    {
        this.players = new ArrayList<Player>(Player.getMAX_PLAYERS());
        this.port = 8303;
    }
    
    public static ArrayList<Server> load()
    {
        ArrayList<Server> al = new ArrayList<Server>();
        
        //load the list of visited servers from .visited file
        try 
        {
            File f = new File("data/.visited");
            if(!f.exists())
                return null;
            
            DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
            BufferedReader br = new BufferedReader(new InputStreamReader(dis));
            
            String line;
            while((line = br.readLine()) != null)
            {
                String [] tab = line.split(" ");
                String ip = tab[0];
                int port = Integer.parseInt(tab[1]);
                boolean favorite = Boolean.parseBoolean(tab[2]);
                
                Server s = new Server(ip, port, favorite);
                al.add(s);
            }
            br.close();
            dis.close();            
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return al;
    }
    
    public static void save(ArrayList<Server> al)
    {
        //save the visited list of servers
        //TODO: improve to update or erase the lines instead of rewriting it all each time you save
        if(al != null && !al.isEmpty())
        {
            try 
            {
                File f = new File("data/.visited");
                if(!f.exists())
                    f.createNewFile();
                
                BufferedWriter bw = new BufferedWriter(new FileWriter("data/.visited", false));
                for(Server s : al)
                {
                    bw.write(s.getIp()+" "+s.getPort()+" "+s.isFavorite());
                    bw.newLine();
                }
                
                bw.flush();
                bw.close(); 
            } 
            catch (FileNotFoundException ex) 
            {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (IOException ex) 
            {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
