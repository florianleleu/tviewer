/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tviewer;
import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author floyd
 */
public class Container extends JPanel
{
    private Thread t1;
    private Thread t2;
    protected Server server;
    protected ArrayList<Server> visited;
    protected URL url;
    protected URLConnection urlcon;
    protected String [] infos;

    public Container()
    {
        t1 = new Thread(new Update());
        t2 = new Thread(new UpdateScreen());
        
        this.infos = new String[10];
        
        this.visited = Server.load();
        this.server = new Server("176.31.63.114");
        
        try 
        {
            this.url = new URL("http://"+Tviewer.TEE_SERVER_API+"/"+this.server.getIp()+"/"+this.server.getPort()+"/xml");    
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
        }   

        t1.start();
        t2.start();
    }
    
    
    public ArrayList<Server> getVisited()
    {
        return this.visited;
    }
    

    
    @Override
    synchronized public void paintComponent(Graphics g)
    {
        
        int y = 10;
        
        for(String info : infos)
        {
            if(info != null)
            {
                g.drawString(info, 10, y);
                
            }   
            y+=10;
        }
    }
    
    synchronized public void update()
    {
        //set parameters for the request
        try 
        {
            urlcon = url.openConnection();
            urlcon.setRequestProperty("user-agent", Tviewer.VERSION+" "+Tviewer.SYSTEM_INFORMATIONS);
            urlcon.connect();  
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //get the informations
        try 
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(this.urlcon.getInputStream()));
            
            //turn the content into a string
            String line, content = "";
            while((line = in.readLine()) != null)
                content += line;

            in.close();        

            //make it a dom document
            DocumentBuilder db;
            try 
            {
                db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = db.parse(new InputSource(new StringReader(content)));

                //print the infos
                Element name = (Element)doc.getElementsByTagName("name").item(0);
                Element map = (Element)doc.getElementsByTagName("map").item(0);
                Element gametype = (Element)doc.getElementsByTagName("gametype").item(0);
                Element ip = (Element)doc.getElementsByTagName("ip").item(0);
                Element port = (Element)doc.getElementsByTagName("port").item(0);
                Element version = (Element)doc.getElementsByTagName("version").item(0);
                Element ping = (Element)doc.getElementsByTagName("ping").item(0);
                Element nb = (Element)doc.getElementsByTagName("playerInGame").item(0);

                Element [] players = new Element[16];

                NodeList nl = doc.getElementsByTagName("players");
                for(int i = 0 ; i < nl.getLength() ; i++)
                {
                    Element e = (Element)nl.item(i);
                    e = (Element)e.getElementsByTagName("name").item(0);
                    //System.out.println(e.getTextContent());
                }

                
                infos[0] = name.getTextContent();
                infos[1] = map.getTextContent();
                infos[2] = gametype.getTextContent();
                infos[3] = ip.getTextContent();
                infos[4] = port.getTextContent();
                infos[5] = version.getTextContent();
                infos[6] = ping.getTextContent();
                infos[7] = nb.getTextContent();
            } 
            catch (ParserConfigurationException ex) 
            {
                Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (SAXException ex) 
            {
                    Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    class Update implements Runnable
    {
        @Override
        public void run() 
        {
            while(true)
            {
                update();
                
                try 
                {
                    Thread.sleep(1000);
                } 
                catch (InterruptedException ex) 
                {
                    Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
    class UpdateScreen implements Runnable
    {
        @Override
        public void run() 
        {
            while(true)
            {
                repaint();
                
                try 
                {
                    Thread.sleep(300);
                } 
                catch (InterruptedException ex) 
                {
                    Logger.getLogger(Container.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
}
