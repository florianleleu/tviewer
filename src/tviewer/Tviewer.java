/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tviewer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;


/**
 *
 * @author floyd
 */
public class Tviewer extends JFrame
{
    protected final static String VERSION = "Tviewer/1.0.0";
    protected final static String SYSTEM_INFORMATIONS = "("+System.getProperty("os.name")+"; "+System.getProperty("os.version")+"; "+System.getProperty("os.arch")+")";
    protected final static String TEE_SERVER_API = "176.31.63.114";
    
    protected JMenuBar menuBar = new JMenuBar();
    protected JMenu menu = new JMenu("File");
    protected JMenuItem item1 = new JMenuItem("Start");
    protected JMenu servers = new JMenu("Server");
    protected JMenuItem item2 = new JMenuItem("Exit");
    protected JRadioButtonMenuItem jrmi1 = new JRadioButtonMenuItem("176.31.63.114:8303");
    protected JRadioButtonMenuItem jrmi2 = new JRadioButtonMenuItem("176.31.63.114:8304");

    protected Container container;
    protected static SystemTray tray;

    public Tviewer()
    {
        this.container = new Container();
        this.setContentPane(this.container);  
                
        //add menu start
        this.menu.add(item1);
        
        //add the servers
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrmi1);
        bg.add(jrmi2);

        ActionListener sliceActionListener = new ActionListener() 
        {
            public void actionPerformed(ActionEvent actionEvent) {
                AbstractButton aButton = (AbstractButton) actionEvent.getSource();
                System.out.println("Selected: " + aButton.getText());
            }
        };
        
        jrmi1.addActionListener(sliceActionListener);
        jrmi2.addActionListener(sliceActionListener);
        
        //On présélectionne la première radio
        jrmi1.setSelected(true);
        this.servers.add(jrmi1);
        this.servers.add(jrmi2);
        
        //add menu servers
        this.menu.add(servers);
        
        //add menu exit
        item2.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
               
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
            }
        });

        this.menu.add(item2);
        
        //add menu bar to the menu
        this.menuBar.add(menu);

        this.setJMenuBar(menuBar);
        
        this.setTitle(Tviewer.VERSION);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        
        try 
        {
            this.setIconImage(ImageIO.read(new File("data/icon.png")));
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Tviewer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /*
        //set the system tray
        if(SystemTray.isSupported())
        {
            Tviewer.tray =  SystemTray.getSystemTray();

            PopupMenu popup = new PopupMenu();
            MenuItem mItem1 = new MenuItem("Exit");
            
            mItem1.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e) 
                {                   
                    System.exit(0);
                }
            });
            
            popup.add(mItem1);


            
            TrayIcon trayIcon = null;
            try 
            {
                trayIcon = new TrayIcon(ImageIO.read(new File("data/icon.png")), "Tviewer", popup);
                trayIcon.setImageAutoSize(true);
                Tviewer.tray.add(trayIcon);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Tviewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (AWTException ex) 
            {
                Logger.getLogger(Tviewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            trayIcon.displayMessage("Tviewer", Tviewer.VERSION+" is working fine.", TrayIcon.MessageType.INFO);    
            trayIcon.setToolTip("Tviewer : follow in real time a server");
        }
        */
    }
    
    public void exit()
    {
        Server.save(this.container.getVisited());
        System.exit(0);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Tviewer t = new Tviewer();
    }
}
