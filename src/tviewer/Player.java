/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tviewer;

/**
 *
 * @author floyd
 */
public class Player 
{
    protected final static int MAX_PLAYERS = 16;
    
    protected String name;
    protected String clan;
    protected String country;
    protected int score;
    protected int status;
    
    public Player()
    {
        
    }

    public static int getMAX_PLAYERS() 
    {
        return MAX_PLAYERS;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
}
